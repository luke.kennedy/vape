package service

import (
	"net"
	"net/http"
)

// Run runs the service
func Run() error {
	// TODO: configure address and port
	l, err := net.Listen("tcp", "0.0.0.0:0")
	if err != nil {
		return err
	}
	return http.Serve(l, &webserver{})
}
