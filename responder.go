package vape

import (
	"context"
	"crypto"
	"crypto/x509"
	"encoding/base64"
	"fmt"
	"hash"
	"net/url"

	"golang.org/x/crypto/ocsp"
)

// Responder is an OCSP responder
type Responder struct {
	AuthoritiesByNameHash map[string]*Authority
	AuthoritiesByKeyHash  map[string]*Authority
}

// Authority is a CA/VA cert or cert pair.
type Authority struct {
	AuthorityCert *x509.Certificate
	SignerCert    *x509.Certificate
	cRLDPs        []*url.URL
	Key           crypto.Signer
}

func (a *Authority) validate(ctx context.Context, request *ocsp.Request) ([]byte, error) {
	return nil, fmt.Errorf("not implemented")
}

// NewResponder creates a new OCSP responder
func NewResponder(authorities []*Authority) (*Responder, error) {
	r := &Responder{}
	algorithms := map[crypto.Hash]hash.Hash{
		crypto.MD4:         crypto.MD4.New(),
		crypto.MD5:         crypto.MD5.New(),
		crypto.SHA1:        crypto.SHA1.New(),
		crypto.SHA224:      crypto.SHA224.New(),
		crypto.SHA256:      crypto.SHA256.New(),
		crypto.SHA384:      crypto.SHA384.New(),
		crypto.SHA512:      crypto.SHA512.New(),
		crypto.MD5SHA1:     crypto.MD5SHA1.New(),
		crypto.RIPEMD160:   crypto.RIPEMD160.New(),
		crypto.SHA3_224:    crypto.SHA3_224.New(),
		crypto.SHA3_256:    crypto.SHA3_256.New(),
		crypto.SHA3_384:    crypto.SHA3_384.New(),
		crypto.SHA3_512:    crypto.SHA3_512.New(),
		crypto.SHA512_224:  crypto.SHA512_224.New(),
		crypto.SHA512_256:  crypto.SHA512_256.New(),
		crypto.BLAKE2s_256: crypto.BLAKE2s_256.New(),
		crypto.BLAKE2b_256: crypto.BLAKE2b_256.New(),
		crypto.BLAKE2b_384: crypto.BLAKE2b_384.New(),
		crypto.BLAKE2b_512: crypto.BLAKE2b_512.New(),
	}
	for _, authority := range authorities {
		for _, crldp := range authority.AuthorityCert.CRLDistributionPoints {
			parsed, err := url.Parse(crldp)
			if err != nil {
				return nil, err
			}
			authority.cRLDPs = append(authority.cRLDPs, parsed)
		}
		// Build all supported hash -> authority mappings, for maximum performance
		for algName, alg := range algorithms {
			// Name
			alg.Reset()
			_, err := alg.Write(authority.AuthorityCert.RawIssuer)
			if err != nil {
				return nil, err
			}
			r.AuthoritiesByNameHash[hashString(algName, alg.Sum(nil))] = authority
			// Key
			alg.Reset()
			_, err = alg.Write(authority.AuthorityCert.RawSubjectPublicKeyInfo)
			if err != nil {
				return nil, err
			}
			r.AuthoritiesByNameHash[hashString(algName, alg.Sum(nil))] = authority
		}
	}
	return r, nil
}

func hashString(hashAlg crypto.Hash, hashedData []byte) string {
	return hashAlg.String() + base64.StdEncoding.EncodeToString(hashedData)
}

// Respond handles an OCSP request
func (r *Responder) Respond(ctx context.Context, request *ocsp.Request) ([]byte, error) {
	authority, ok := r.AuthoritiesByNameHash[hashString(request.HashAlgorithm, request.IssuerNameHash)]
	if ok {
		return authority.validate(ctx, request)
	}
	authority, ok = r.AuthoritiesByKeyHash[hashString(request.HashAlgorithm, request.IssuerKeyHash)]
	if ok {
		return authority.validate(ctx, request)
	}
	// Not authorized for this request
	return ocsp.CreateResponse(nil, nil, ocsp.Response{}, nil)
}
