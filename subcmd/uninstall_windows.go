package subcmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// AddUninstall adds the install command
func AddUninstall(rootCmd *cobra.Command) {
	uninstallCmd := &cobra.Command{
		Use:   "uninstall",
		Short: "Uninstalls vape",
		RunE: func(cmd *cobra.Command, args []string) error {
			return fmt.Errorf("not implemented")
		},
	}
	rootCmd.AddCommand(uninstallCmd)
}
