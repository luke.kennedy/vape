package subcmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/luke.kennedy/vape/internal/service"
)

// AddStart adds the start command
func AddStart(rootCmd *cobra.Command) {
	startCmd := &cobra.Command{
		Use:   "start",
		Short: "Starts vape without any in-built service wrapper",
		RunE: func(cmd *cobra.Command, args []string) error {
			return service.Run()
		},
	}
	rootCmd.AddCommand(startCmd)
}
