package subcmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// AddInstall adds the install command
func AddInstall(rootCmd *cobra.Command) {
	installCmd := &cobra.Command{
		Use:   "install",
		Short: "Installs vape",
		RunE: func(cmd *cobra.Command, args []string) error {
			return fmt.Errorf("not implemented")
		},
	}
	rootCmd.AddCommand(installCmd)
}
