package subcmd

import "github.com/spf13/cobra"

// AddCommands adds all commands to the sub-command
func AddCommands(rootCmd *cobra.Command) {
	AddInstall(rootCmd)
	AddUninstall(rootCmd)
	AddStart(rootCmd)
}
