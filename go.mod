module gitlab.com/luke.kennedy/vape

go 1.16

require (
	github.com/spf13/cobra v1.2.1
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
)
