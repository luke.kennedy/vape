# VAPE

Validation Authority's Pretty Easy

## Mission Statement

Validation Authorities suck. It's a pretty simple job, but somehow the big players keep messing it up. I reckon I can do better, so I'm giving it a go.

A successful VA needs to:

- Access a CA's CRL on a regular basis, or allow operators to replace this CRL manually when required.
- Handle incoming OCSP requests.
- Allow clear configuration or documentation of the relationship between the first two steps.

This is NOT a Certificate Authority. You can use any CA you like if you're providing the CRL yourself, and if the CRL is available via HTTP(S) or LDAP(S) then any CA should also work.