package main

import (
	"log"

	"github.com/spf13/cobra"
	"gitlab.com/luke.kennedy/vape/subcmd"
)

func main() {
	rootCmd := &cobra.Command{
		Use:   "vape",
		Short: "base command for the VA",
	}
	subcmd.AddCommands(rootCmd)
	err := rootCmd.Execute()
	if err != nil {
		log.Fatalln(err)
	}
}
